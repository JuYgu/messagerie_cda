
insert into utilisateur 
values 
(nextval('utilisateur_seq'), 'Reb12', 'reb12@hotmail.fr', '0102030405', 'Azer', 'Reb1234', 'Rebecca'),
(nextval('utilisateur_seq'),'Jo01', 'jo01@hotmail.fr', '0102030512', 'Maur', 'Jo1234', 'Joris'),
(nextval('utilisateur_seq'),'Adri', 'adri@hotmail.fr', '0321030405', 'Mauraer', 'Adri1234', 'Adrien'),
(nextval('utilisateur_seq'),'Ant12', 'ant@hotmail.fr', '7878787878', 'Mouchin', 'Ant123', 'Antoine'),
(nextval('utilisateur_seq'),'Jean12', 'jean@hotmail.fr', '1234030405', 'Bobo', 'Jean1234', 'Jean'),
(nextval('utilisateur_seq'),'Thom12', 'thom@hotmail.fr', '0102030405', 'Mazzini', 'Thom1234', 'Thomas'),
(nextval('utilisateur_seq'),'Moh12', 'moh@hotmail.fr', '0102030409', 'Benjira', 'Moh1234', 'Mohammed'),
(nextval('utilisateur_seq'),'Nico12', 'nico@hotmail.fr', '1234567890', 'Igel', 'Nico1234', 'Nicolas');

insert into message
values
(nextval('message_seq'), 'Salut, tu vas bien, je reviens vers toi concernant le plat que j ai laisse chez toi samedi soir. Des bises', false, 'Le plat', 1,8),
(nextval('message_seq'), 'Bonjour, je ne sais pas si tu as vu la maniere dont thom s est adresse a moi ce matin, mais la prochaine fois que �a arrive, je me la fait !', false, 'GRAVE', 2,3),
(nextval('message_seq'), 'Merci pour le steack de la derniere fois. L assise est pas mal et le dossier est confortable.', false, 'steak', 2,8),
(nextval('message_seq'), 'Une bouteille d huile, des pates, du papier, du beurre et un transat', false, 'liste', 8,5),
(nextval('message_seq'), 'Si jamais tu recommences, je t assure thom que tu vas le regretter', false, 'arrete maintenant', 2,6),
(nextval('message_seq'), 'On va boire un coup jeudi, ca te tente ?', false, 'boire un pot', 7,4),
(nextval('message_seq'), 'Ya antoine mouchin qui m a invite a boire un coup, tu connais ce type ?', false, 'mec chelou', 4,3),
(nextval('message_seq'), 'ouaip, c est un gars qui vient de Muchin sur Loize. Il est sympa mais un peu bete', false, 'mec chelou', 3,4),
(nextval('message_seq'), 'J ai pas de news depuis longtime, ca va ?', false, 'nouvelles', 3,8),
(nextval('message_seq'), 'ouais, un peu mal au dos en ce moment, mais ca va.', false, 'nouvelles', 8,3);