cda-projet-messagerie-[AJT]
_________________________________________________________________________________________
Mini-projet messagerie CDA 2020 - Roubaix.


Finalité de l'application :
_________________________________________________________________________________________
Création d'une messaerie interne à destination des étudiants CDA 2020/2021
L'application doit pouvoir proposer les fonctionnalités suivantes :
- authentification à l'application de messagerie
- création de compte utilisateur
- accès à la boîte de réception
- lecture des message
- écriture de nouveaux messages
- menu pour créer un nouveau message, modifier le profil utilisateur & déconnexion


Suivi du projet :
_________________________________________________________________________________________
Le tableau de bord du projet est disponible sur le lien suivant :
https://trello.com/b/rNxVZBmF/cda-projet-messagerie-ajt


Conception du projet :
_________________________________________________________________________________________
Les diagrammes UML (de cas d'utilisation, de séquence et de classes) sont également
disponibles sur le tableau de bord Trello.


Démarche suivie :
_________________________________________________________________________________________
Lecture du projet et assimilation des concepts attendus.
Rencontre entre les contributeurs pour une mise en commun de la compréhension du projet.
Conception des diagrammes UML de cas d'utilisation, de séquence et de classes en trinôme.
Création d'un tableau de bord Trello.
Création du repository et partage via BitBucket.
Partage des tâches et mise en commun via GitKraken.


Pré-requis :
_________________________________________________________________________________________
Création de la base de données et de l'utilisateur
Créez un utilisateur de base de données sur pgAdmin que vous appélerez ajt.
Attribuez-lui le mot de passe 123 et définissez-le comme un superuser.
Créez une base de données que vous appellerez "messagerie-cda", dont le propriétaire 
sera l'utilisateur ajt

Création des tables et entrées :
Exécutez ensuite le script SQL disponible sur le fichier dans le dossier 8 - Script SQL :
ScriptSQL-cda-projet-messagerie-[MAZZINI].sql

Lancez l'application grâce au fichier .bat fourni.
Identifiez-vous ou créez un nouveau compte utilisateur.
Laissez-vous guider pour la suite.

Voici les fonctionnalités disponibles :
- créer un compte utilisateur
- se connecter à l'application
- visualiser sa boîte de réception
- visualiser les messages reçus et envoyés
- envoyer un nouveau message
- éditer le profil de l'utilisateur
- se déconnecter de l'application


Contributeurs :
_________________________________________________________________________________________
Adrien Maurer
Juliette Yguel
Thomas Mazzini