package fr.afpa.messagerie_cda;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.Transaction;

import fr.afpa.messagerie_cda.beans.Message;
import fr.afpa.messagerie_cda.beans.Utilisateur;
import fr.afpa.messagerie_cda.session.HibernateUtils;

public class Main {
	
	public static Utilisateur userBase;

	public static void main(String[] args) {

  	  Session s = HibernateUtils.getSession();
  	  Transaction tx = s.beginTransaction();
  	  tx.commit();
	}
}