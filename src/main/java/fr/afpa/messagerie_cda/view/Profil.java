package fr.afpa.messagerie_cda.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.MenuItem;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.Border;

import fr.afpa.messagerie_cda.Main;
import fr.afpa.messagerie_cda.control.ControlInterface;
import fr.afpa.messagerie_cda.control.ControlMessage;
import fr.afpa.messagerie_cda.control.ControlUtilisateur;
import lombok.Getter;
import lombok.Setter;

public class Profil extends JFrame implements ActionListener, ControlInterface {
	
	@Getter
	@Setter
	private boolean update;
	
	JPanel container;
	
	JLabel labPhoto;
	ImageIcon photo;
	
	JPanel contProfil;
	JPanel contLoginPass;
		JPanel contLog;
		JLabel log;
		JTextField textLog;

		JPanel contPass;
		JLabel pass;
		JTextField textPass;
	
		JPanel champsProfil;
			JPanel contNom;
				JLabel textNom;
				JTextField nom;
			JPanel contPrenom;
				JLabel textPrenom;
				JTextField prenom;
			JPanel contMail;
				JLabel textMail;
				JTextField mail;
			JPanel contNoPhone;
				JLabel textNoPhone;
				JTextField noPhone;
	
	JPanel contButton;
		JButton btnModif;
		JButton btnReturnMessagerie;
	
	public Profil () {
		
		container = new JPanel( new GridLayout(3,1));
		container.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
		
		this.add(container);
		
			//IMPLEMENTATION PROFIL
				contProfil = new JPanel(new GridLayout(3,2));
				//contProfil.setBackground(Color.black);
					/*implementation login, password*/

						//Login / Password
						contLog = new JPanel(new GridLayout(2,1));
							log = new JLabel("Login");
							textLog = new JTextField(30);
							textLog.setEditable(false);
						contLog.add(log); contLog.add(textLog);
						
						contPass = new JPanel(new GridLayout(2,1));
							pass = new JLabel("Password");
							textPass = new JTextField(30);
							textPass.setEditable(false);
						contPass.add(pass); contPass.add(textPass);
										
					
				contProfil.add(contLog);contProfil.add(contPass);
			
				/*Implementation champs */

						contNom = new JPanel(new GridLayout(2,1));
							textNom = new JLabel("Nom");
							nom = new JTextField(30);
						contNom.add(textNom); contNom.add(nom);
							
						contPrenom = new JPanel(new GridLayout(2,1));
							textPrenom = new JLabel("Prenom");
							prenom = new JTextField(30);
						contPrenom.add(textPrenom);contPrenom.add(prenom);
						
						contMail = new JPanel(new GridLayout(2,1));
							textMail = new JLabel("Mail");
							mail = new JTextField(30);
							mail.setEditable(false);
						contMail.add(textMail); contMail.add(mail);
							
						contNoPhone = new JPanel(new GridLayout(2,1));
							textNoPhone = new JLabel("N° de Téléphone");
							noPhone = new JTextField(30);
						contNoPhone.add(textNoPhone); contNoPhone.add(noPhone);
					
					contProfil.add(contNom);	contProfil.add(contPrenom);  contProfil.add(contMail);	contProfil.add(contNoPhone);

			container.add(contProfil);
					
				/*Implementation des boutons de modification et retour messagerie*/
					contButton = new JPanel(new GridLayout(3,4));
						btnModif= new JButton("Modifier");
						if(update) {
							btnModif= new JButton("Valider");
						}
						btnReturnMessagerie = new JButton("Retour sur la messagerie");
						btnReturnMessagerie.setName("retour");
					contButton.add(Box.createGlue());contButton.add(Box.createGlue());contButton.add(Box.createGlue());	contButton.add(Box.createGlue());	
					contButton.add(Box.createGlue());contButton.add(btnModif);contButton.add(btnReturnMessagerie);contButton.add(Box.createGlue());
					contButton.add(Box.createGlue());contButton.add(Box.createGlue());contButton.add(Box.createGlue());contButton.add(Box.createGlue());	
					
					btnModif.addActionListener(this);
					btnReturnMessagerie.addActionListener(this);
				
			container.add(contButton);
		
		init();
		refresh(update);
	}
	
	
	/**
	 * Permet d'initialiser la fenetre
	 */
	public  void init() {
		/*INITAaTIONATION PARAM FENETRE*/
		//this.setUndecorated(true);
		this.setSize(1000, 750);
		this.setVisible(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// Récupération de la resolution de l'écran
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		this.setLocation((screen.width - this.getSize().width)/2,(screen.height - this.getSize().height)/2);
	}

	
	/**
	 * Permet de rafraichir la fenetre en fonction du click modification ou valider
	 * @param update
	 */
	public void refresh(boolean update) {
		new ControlMessage();
		textLog.setText(MoteurMessagerie.currentUser.getLogin());
		textPass.setText(MoteurMessagerie.currentUser.getPassword());
		nom.setText(MoteurMessagerie.currentUser.getNom());
		prenom.setText(MoteurMessagerie.currentUser.getPrenom());
		mail.setText(MoteurMessagerie.currentUser.getMail());
		noPhone.setText(MoteurMessagerie.currentUser.getMail());
		if (!update) {
			textPass.setEditable(false);
			nom.setEditable(false);
			prenom.setEditable(false);
			noPhone.setEditable(false);
			btnModif.setName("Modifier");
			btnModif.setText("Modifier");
			
		}else {
			textPass.setEditable(true);
			nom.setEditable(true);
			prenom.setEditable(true);
			noPhone.setEditable(true);
			btnModif.setName("Valider");
			btnModif.setText("Valider");
		}		
	}
	
	/**
	 * Permet de modifier l'user en base de donnée 
	 * @return
	 */
	 public boolean updateUser() {
		ControlUtilisateur cUser =  new ControlUtilisateur ();
		return cUser.checkSyntax(textPass.getText(),	nom.getText(),prenom.getText(),	noPhone.getText());
	 }

	
	public static void main(String [] args) {
		new Profil();
	}
	
	/*--------------------------------------------------------------------METHODES D'ACTION PERFORMED-------------------------------------------------------*/
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JButton) {
			actionButton(e);
		} 
	}
	
	/**
	 * Permet d'activer les evènements des bouton. Appelé dans ActionPerformed
	 * @param e
	 */
	public  void actionButton(ActionEvent e) {
		if("Modifier".equals(((JButton)e.getSource()).getName())) {
			refresh(true);
		}else if("Valider".equals(((JButton)e.getSource()).getName())) {
			updateUser();
			refresh(false);
		}
		
		if("retour".equals(((JButton)e.getSource()).getName())) {
			ControlInterface.goToProfilToMessagerie(this, MoteurMessagerie.messagerieUser);
		}
	}
}