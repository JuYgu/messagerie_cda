package fr.afpa.messagerie_cda.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import fr.afpa.messagerie_cda.Main;
import fr.afpa.messagerie_cda.beans.Message;
import fr.afpa.messagerie_cda.control.ControlInterface;
import fr.afpa.messagerie_cda.control.ControlMessage;
import fr.afpa.messagerie_cda.control.ControlUtilisateur;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReadingMessage extends JFrame implements ActionListener {

	public static Message currentMsg;
	 
	// Mise en place des panels
	private JPanel fenetre;
	
	private JPanel nomPrenom;
	private JLabel inNomPrenom;
	
	private JPanel choixMenu;
	private JButton inbox, logout;
	
	private JPanel viewingMessage;
	private JTextField sender, object;
	private JTextArea corps;
	
	private String emetteur;
	private String corpsText;
	private String objectText;
	
	/**
	 * @author Thomas
	 * frame qui permet de visualiser un message dans la boîte de réception
	 */
	public ReadingMessage() {
		super("CDA Messagerie");
		
		// préparation de la fenêtre
		fenetre = new JPanel(new BorderLayout());
		this.add(fenetre);
		
		ControlMessage control = new ControlMessage();
		System.out.println(Main.userBase.getPrenom());
		
		// affichage du bonjour utilisateur
		nomPrenom = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 50));
		//nomPrenom.setBackground(Color.GRAY);
		inNomPrenom = new JLabel("Bonjour Nom Prenom");
		inNomPrenom.setToolTipText("Bonjour Nom Prenom");
		inNomPrenom.setFont(new java.awt.Font(Font.SERIF, Font.BOLD, 30));
		
		inNomPrenom.setText("Bonjour "+Main.userBase.getPrenom()+" "+Main.userBase.getNom());
		
		inbox = new JButton("Boîte de réception");
		inbox.setName("Messagerie");
		inbox.setOpaque(false);
		inbox.setBorderPainted(false);
		inbox.addActionListener(this);
		logout = new JButton("Deconnexion");
		logout.setOpaque(false);
		logout.setBorderPainted(false);
		logout.addActionListener(this);
		
		nomPrenom.add(inNomPrenom);
		
		nomPrenom.add(inbox);
		nomPrenom.add(logout);
		fenetre.add(nomPrenom,BorderLayout.NORTH);	
			
		viewingMessage = new JPanel(new FlowLayout(FlowLayout.CENTER, 0, 100));
		sender = new JTextField("Emetteur",20);
		sender.setEditable(false);
		object = new JTextField("Objet",42);
		object.setEditable(false);
		corps = new JTextArea("Corps", 20, 50);
		corps.setEditable(false);
		viewingMessage.add(sender);
		viewingMessage.add(object);
		viewingMessage.add(corps);
		fenetre.add(viewingMessage,BorderLayout.CENTER);
		
		init();
	}
	
	public void init() {
		/*INITATIONATION PARAM FENETRE*/
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(1000, 750);
		this.setVisible(false);
		// Récupération de la resolution de l'écran
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		this.setLocation((screen.width - this.getSize().width)/2,(screen.height - this.getSize().height)/2);
	}
	
	public void initMsg() {
		sender.setText(getEmetteur());
		object.setText(getObjectText());
		corps.setText(getCorpsText());
	}
	
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		System.out.println(e.getSource());
		if ( "Messagerie".equals( ((JButton)e.getSource()).getName()) ) {
			//System.out.println("Méthode pour renvoyer la slide Boîte de réception");
			ControlInterface.goToReadMessToMessagerie(this, MoteurMessagerie.messagerieUser);
		} else if ( "Deconnexion".contentEquals( ((JButton)e.getSource()).getText()) ) {
			ControlInterface.goToMessageToAuth(this, MoteurMessagerie.auth);
		}
	}
}