package fr.afpa.messagerie_cda.view;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import fr.afpa.messagerie_cda.control.ControlUtilisateur;

public class CreationDeCompte extends JFrame implements WindowListener, ActionListener, KeyListener {
//Creation des attributs Boutons
	JButton valider;
	
//Creation des TextField
	JTextField nom;
	JTextField prenom;
	JTextField mail;
	JTextField numTel;
	JTextField login;
	JTextField mdp;
//Creation des Labels
	
	JLabel vide;
	JLabel name;
	JLabel surname;
	JLabel message;
	JLabel tel;
	JLabel log;
	JLabel password;
// Creation des Pannels
	JPanel p;
	JPanel p1;
	JPanel p2;
	JPanel p3;
	JPanel p4;
	JPanel p5;

	public CreationDeCompte() throws HeadlessException {
		super("CDA Messagerie");
	//Instanciation du bouton
		this.valider=new JButton("Valider");
		
		//Instanciation des TextFields
		this.nom=new JTextField(30);
		this.prenom=new JTextField(30);
		this.mail=new JTextField(50);
		this.numTel=new JTextField(15);
		this.login=new JTextField(10);
		this.mdp=new JTextField(20);
		//Instanciation des labels
		this.vide=new JLabel("");
		this.log=new JLabel("Login");
		this.message=new JLabel("Mail");
		this.name=new JLabel("Nom");
		this.tel=new JLabel("Num téléphone");
		this.surname=new JLabel("Prénom");
		this.password=new JLabel("Mot de passe");
		//instanciation des panels
		this.p=new JPanel();
		this.p1=new JPanel();
		this.p2=new JPanel();
		this.p3=new JPanel();
		this.p4=new JPanel();
		this.p5=new JPanel();
		//Fermeture de la fenetre avec WindowsListener
		
		//Decoupade de la frame
		
		
		this.setLayout(new BorderLayout());
		p.setLayout(new BorderLayout());
		
		p2.add(Box.createGlue());
		p3.add(Box.createGlue());
		p4.add(Box.createGlue());
		p5.add(Box.createGlue());
		p.add(p2,BorderLayout.WEST);
		p.add(p3,BorderLayout.EAST);
		p.add(p3,BorderLayout.NORTH);
		p.add(p5,BorderLayout.SOUTH);
		
		TitledBorder envoye = BorderFactory.createTitledBorder("Création de compte");
		p.setBorder(envoye);
		p1.setLayout(new GridLayout(7,2));
		
		p1.add(name);
		p1.add(nom);
		p1.add(surname);
		p1.add(prenom);
		p1.add(message);
		p1.add(mail);
		p1.add(tel);
		p1.add(numTel);
		p1.add(log);
		p1.add(login);
		p1.add(password);
		p1.add(mdp);
		p1.add(vide);
		p1.add(valider);

		p.add(p1,BorderLayout.CENTER);
		this.add(p,BorderLayout.CENTER);
		
		this.setSize(1000, 750);
		this.setVisible(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// Récupération de la resolution de l'écran
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		this.setLocation((screen.width - this.getSize().width)/2,(screen.height - this.getSize().height)/2);
		valider.addActionListener(this);
	}

	@Override
	public void keyPressed(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		boolean verif = false;
	
		if (((JButton) e.getSource()).getText().equalsIgnoreCase("Valider")) {
			
			ControlUtilisateur cu = new ControlUtilisateur();
		verif=cu.controlVerifSyntax(nom.getText(), prenom.getText(), mail.getText(), numTel.getText(), login.getText(), mdp.getText());
		
		if (verif) {
			if(MoteurMessagerie.creaCompteSuccess!=null)
			{
				MoteurMessagerie.creaCompteSuccess.setVisible(true);
			}
			else
			{
				MoteurMessagerie.creaCompteSuccess = new CreationDeCompteReussi();
				MoteurMessagerie.creaCompteSuccess.setVisible(true);
			}
			this.setVisible(false);

		}else {System.out.println("Veuillez remplir les champs");}
		}
		
	}

	@Override
	public void windowActivated(WindowEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowClosed(WindowEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowClosing(WindowEvent arg0) {
		System.exit(0);

	}

	@Override
	public void windowDeactivated(WindowEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowDeiconified(WindowEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowIconified(WindowEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowOpened(WindowEvent arg0) {
		// TODO Auto-generated method stub

	}
public static void  main(String [] args) {
	CreationDeCompte creaCompte = new CreationDeCompte();
	
}
}