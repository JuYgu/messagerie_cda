package fr.afpa.messagerie_cda.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import fr.afpa.messagerie_cda.control.ControlInterface;
import fr.afpa.messagerie_cda.control.ControlMessage;

public class EnvoyerMessageReussi extends JFrame implements ActionListener, KeyListener, WindowListener {
// composants
JButton retourPageAuth;
JLabel texConf;

//Creation des Pannels
	JPanel p;
	JPanel p1;
	JPanel p2;
	JPanel p3;
	JPanel p4;
	JPanel p5;
	JPanel cont;

//messages
private static String titreAppli="CDA Messagerie";
private static String labelConfirm="Votre message a été crée avec succès";
private static String buttonBack="Retour messagerie";
	
	public EnvoyerMessageReussi() throws HeadlessException {
	super(titreAppli);
	
	this.texConf=new JLabel(labelConfirm);
	this.retourPageAuth=new JButton(buttonBack);
	
	//instanciation des panels
	this.p=new JPanel();
	this.p1=new JPanel();
	this.p2=new JPanel();
	this.p3=new JPanel();
	this.p4=new JPanel();
	this.p5=new JPanel();
	
	//Decoupade de la frame
	
	
	this.setLayout(new BorderLayout());
	p.setLayout(new BorderLayout());
	
	p2.add(Box.createGlue());
	p3.add(Box.createGlue());
	p4.add(Box.createGlue());
	p5.add(Box.createGlue());
	p.add(p2,BorderLayout.WEST);
	p.add(p3,BorderLayout.EAST);
	p.add(p3,BorderLayout.NORTH);
	p.add(p5,BorderLayout.SOUTH);
	
	TitledBorder envoye = BorderFactory.createTitledBorder("Confirmation envoi de mail");
	p.setBorder(envoye);
	p1.setLayout(new GridLayout(10,2));
	
	cont = new JPanel(new GridLayout(1,2));
	cont.add(texConf); cont.add(retourPageAuth);
	p1.add(cont);
	

	p.add(p1,BorderLayout.CENTER);
	this.add(p,BorderLayout.CENTER);
	
	this.setSize(1000, 750);
	this.setVisible(false);
	setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	// Récupération de la resolution de l'écran
	Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
	this.setLocation((screen.width - this.getSize().width)/2,(screen.height - this.getSize().height)/2);
	
	retourPageAuth.addActionListener(this);
	
}

	@Override
	public void windowActivated(WindowEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowClosed(WindowEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowClosing(WindowEvent arg0) {
		// TODO Auto-generated method stub
		System.exit(0);
	}

	@Override
	public void windowDeactivated(WindowEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowDeiconified(WindowEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowIconified(WindowEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowOpened(WindowEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyPressed(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		if ( ((JButton)arg0.getSource()).getText().equals("Retour messagerie")){

				if(MoteurMessagerie.messagerieUser!=null)
				{
					ControlInterface.goToMessageSuccessToMessagerie(this);
					MoteurMessagerie.messagerieUser.setVisible(true);
				}
				else
				{
					MoteurMessagerie.messagerieUser = new MessagerieUtilisateur();
					MoteurMessagerie.messagerieUser.setVisible(true);
				}
				this.setVisible(false);
		}
	}
	
	public static void  main(String [] args) {
		EnvoyerMessageReussi envoyerMessageReussi = new EnvoyerMessageReussi();	
	}
}