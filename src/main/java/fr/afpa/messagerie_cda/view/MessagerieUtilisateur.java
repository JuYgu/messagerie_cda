package fr.afpa.messagerie_cda.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.MenuItem;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.List;

import javax.swing.DefaultListCellRenderer;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.JWindow;
import javax.swing.ListCellRenderer;
import javax.swing.SwingConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import fr.afpa.messagerie_cda.Main;
import fr.afpa.messagerie_cda.beans.Message;
import fr.afpa.messagerie_cda.control.ControlInterface;
import fr.afpa.messagerie_cda.control.ControlMessage;
import lombok.Getter;
import lombok.Setter;

public class MessagerieUtilisateur extends JFrame implements ActionListener, ItemListener, ListSelectionListener, ListCellRenderer, MouseListener{
	
	// Menu
	JMenuBar menu; 
	
	JMenu fichier; 
	JMenuItem deconnexion; JMenuItem newMessage; 
	
	JMenu help; 
	JMenuItem inHelp;
	
	JMenu profil;
	JMenuItem consulter; JMenuItem modifier;
	
	// Contenu
	JPanel contenu;
		//label
	JPanel nomPrenom;
	JLabel inNomPrenom;
		//onglets
	JPanel containerMessagerie;
	JTabbedPane onglets;
	JPanel contlisteReception;
	JPanel contlistSendMsg;
	JPanel contrecherche;
			//elements onglets
	JList<Message> listeReception;
	DefaultListModel<Message> modelReception;
	JList<Message> listeSendMsg;
	
	DefaultListModel<Message> modelSend;
	JPanel contSearchField;
	JLabel labRecherche;
	JTextField textRecherche;
	JButton rechercher;
	String objet;
	JList<Message> recherche;
	DefaultListModel<Message> modelRecherche;
	
	@Getter
	@Setter
	private int UnreadMsg;
	
	public MessagerieUtilisateur() {

		build();
		init();		
	}
	
	/**
	 * Methode servant a la construction de notre fenetre de messagerie.
	 */
	public void build() {
		/*INITIALISATION MENU*/
		
		//A MODIFIER-------------------------------------------------
		ControlMessage control = new ControlMessage();
		//A MODIFIER-------------------------------------------------
		menu = new JMenuBar();
		
		fichier = new JMenu("Fichier");
			deconnexion = new JMenuItem("Déconnexion"); 
			deconnexion.setName("Deconnexion");
		
			newMessage = new JMenuItem("Nouveau message");
			newMessage.setName("Message");

		fichier.add(deconnexion); fichier.add(newMessage);
		
		help = new JMenu("Help");
			inHelp = new JMenuItem("?");
			inHelp.setName("Help");
		help.add(inHelp);
		
		profil = new JMenu("Profil");
			consulter = new JMenuItem("Consulter"); 
			consulter.setName("Consulter");
			modifier = new JMenuItem("Modifier");
			modifier.setName("Modifier");
		profil.add(consulter); profil.add(modifier);
		
		menu.add(fichier); menu.add(help); menu.add(profil);
		
			// Ajouts des actionListener
		deconnexion.addActionListener(this); newMessage.addActionListener(this);help.addActionListener(this);consulter.addActionListener(this);modifier.addActionListener(this);
		
		this.setJMenuBar(menu);
		
		/*INITIALISATION CONTENU*/
		contenu = new JPanel(new BorderLayout());
		this.add(contenu);
		
		/*INITIALISATION LABEL*/
		implementsLabelNomPrenom();
		
		
		/*INITIALISATION ONGLET*/
		containerMessagerie = new JPanel(new FlowLayout());
		JPanel containerOnglets = new JPanel(new FlowLayout(FlowLayout.CENTER, 0, 200));
		containerOnglets.setBackground(Color.ORANGE);
		containerMessagerie.add(containerOnglets);
		
		onglets = new JTabbedPane();
		onglets.setBounds(0,0 ,300, 400);
		
		contlisteReception = new JPanel(new FlowLayout(FlowLayout.CENTER, 0, 50));
		contlistSendMsg = new JPanel(new FlowLayout(FlowLayout.CENTER, 0, 50));
		contrecherche = new JPanel(new FlowLayout(FlowLayout.CENTER, 0, 50));

		onglets.add("Boite de réception ("+control.getCounterUnreadMsg()+")", contlisteReception);
		onglets.add("Boite d'envoi", contlistSendMsg);
		onglets.add("Recherche", contrecherche);
		
		containerMessagerie.add(onglets);
		contenu.add(containerMessagerie, BorderLayout.CENTER);
		
		/*INTEGRATION ELEMENTS ONGLET*/
			//Liste messagerie
			implementsReceptionList();
			implementsSendList();
			implementSearchList();
	}
	
	/**
	 * Initialise la fenetre
	 */
	public  void init() {
		/*INITAaTIONATION PARAM FENETRE*/

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		this.setSize(1000, 750);
		this.setVisible(false);
		
		refresh();
		// Récupération de la resolution de l'écran
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		this.setLocation((screen.width - this.getSize().width)/2,(screen.height - this.getSize().height)/2);
	}
	
	/**
	 * Permet l'impémentation de la boite de réception
	 */
	public void implementsReceptionList() {
		
		// On récupère tous les messages reçu du currentUser
		ControlMessage cMsg = new ControlMessage();
		List<Message> listObjectReceptMsg = cMsg.getAllReceptMsg();
		
		listeReception = new JList();
		listeReception.setVisibleRowCount(5);
		listeReception.setFixedCellWidth(750);
		listeReception.setFixedCellHeight(30);
		
		listeReception.setSelectionBackground(Color.YELLOW);
		listeReception.addListSelectionListener(this);
		listeReception.addMouseListener(this);
		
		DefaultListCellRenderer renderer = (DefaultListCellRenderer) listeReception.getCellRenderer();
		renderer.setHorizontalAlignment(SwingConstants.CENTER);
		modelRecherche = new DefaultListModel<>();

		JLabel labelReceptionList = new JLabel();
		
		modelReception = new DefaultListModel<>();
		for( Message msg : listObjectReceptMsg) {
			modelReception.addElement(msg);
		}
		
		listeReception.setModel(modelReception);
		
		contlisteReception.add(listeReception);
	}
	
	
	/**
	 * Permet l'impementation de la boite d'envoi
	 */
	public void implementsSendList() {
		
		// On récupère tous les messages reçu du currentUser
		
		listeSendMsg = new JList();
		listeSendMsg.addMouseListener(this);
		listeSendMsg.setVisibleRowCount(5);
		listeSendMsg.setFixedCellWidth(750);
		listeSendMsg.setFixedCellHeight(30);
		DefaultListCellRenderer renderer = (DefaultListCellRenderer) listeSendMsg.getCellRenderer();
		renderer.setHorizontalAlignment(SwingConstants.CENTER);

		ControlMessage cMsg = new ControlMessage();
		
		modelSend = new DefaultListModel<>();
		List<Message> listObjectSendMsg = cMsg.getAllSendMsg();
		for( Message msg : listObjectSendMsg) {
			modelSend.addElement(msg);
		}
		
		listeSendMsg.setModel(modelSend);
		
		contlistSendMsg.add(listeSendMsg);
	}
	
	/**
	 * Permet l'implementation de la boite de recherche
	 */
	public void implementSearchList() {
		
		// On récupère tous les messages reçu du currentUser
		contSearchField = new JPanel(new FlowLayout());
		labRecherche = new JLabel("Recherchez par Objet");
		rechercher = new JButton("Rechercher");
		textRecherche = new JTextField(50);
		
		JPanel s = new JPanel(new GridLayout(2,1));
		recherche = new JList();
		recherche.addMouseListener(this);
		recherche.setVisibleRowCount(5);
		recherche.setFixedCellWidth(750);
		recherche.setFixedCellHeight(30);
		DefaultListCellRenderer renderer = (DefaultListCellRenderer) recherche.getCellRenderer();
		renderer.setHorizontalAlignment(SwingConstants.CENTER);
		
		//Ecouteur d'evenement sur le TextField
		textRecherche.addActionListener(this);
		rechercher.addActionListener(this);
		

		JLabel labelSendList = new JLabel();
		 
		contSearchField.add(labRecherche);contSearchField.add(textRecherche); contSearchField.add(rechercher); 
		s.add(contSearchField); s.add(recherche);
		contrecherche.add(s);
	}
	
	/**
	 * Permet le refresh de la boite de recherche après chaque recherche
	 */
	public void implementsRecherche() {
		ControlMessage cMsg = new ControlMessage();
		List<Message> listObjectSearchMsg = cMsg.CgetSearchingMsg(objet);
		System.out.println(listObjectSearchMsg);
		modelRecherche.clear();
		for( Message msg : listObjectSearchMsg) {
			modelRecherche.addElement(msg);
		}
		recherche.setModel(modelRecherche);
	}
	
	/**
	 * d'initaliser et Permet de rendre dynamique le label de bienvenue
	 */
	public void implementsLabelNomPrenom() {
		nomPrenom = new JPanel(new FlowLayout(FlowLayout.CENTER, 0, 50));
		//nomPrenom.setBackground(Color.GRAY);
		inNomPrenom = new JLabel("Bienvenue Nom Prenom");
		inNomPrenom.setToolTipText("Bienvenue NOm Prenom");
		inNomPrenom.setFont(new java.awt.Font(Font.SERIF, Font.BOLD, 30));
		inNomPrenom.setText("Bienvenue "+MoteurMessagerie.currentUser.getPrenom()+" "+MoteurMessagerie.currentUser.getNom());
		
		nomPrenom.add(inNomPrenom);
		contenu.add(nomPrenom,BorderLayout.NORTH);
	}
	
	public void refresh() {
		ControlMessage control = new ControlMessage();
		this.setUnreadMsg(control.getCounterUnreadMsg());
		System.out.println("UndearMesg"+UnreadMsg);
		ControlMessage cMsg = new ControlMessage();
		
		//Refresh listeReception
		List<Message> listObjectReceptMsg = cMsg.getAllReceptMsg();
		modelReception.clear();
		for( Message msg : listObjectReceptMsg) {
			System.out.println(msg);
			modelReception.addElement(msg);
		}
		listeReception.setModel(modelReception);
		
		//Refresh send mail
		modelSend.clear();
		List<Message> listObjectSendMsg = cMsg.getAllSendMsg();
		for( Message msg : listObjectSendMsg) {
			modelSend.addElement(msg);
		}
		listeSendMsg.setModel(modelSend);
	}

	/*--------------------------------------------------------------------METHODES D'ACTION PERFORMED-------------------------------------------------------*/
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JButton) {
			actionButton(e);
		}else if (e.getSource() instanceof JMenuItem) {
			actionMenuItem(e);
		}
	}
	
	public  void actionButton(ActionEvent e) {
		if( "Rechercher".equals(((JButton) e.getSource()).getText()) ) {
			objet = textRecherche.getText();
			System.out.println("L'objet recherché :"+objet);
			implementsRecherche();
		}
	}
	
	public  void actionMenuItem(ActionEvent e) {
		
		if( "Deconnexion".equals(((JMenuItem) e.getSource()).getName()) ) {
			ControlInterface.goToMessagegieToAuth(this);
		}else if( "Message".equals(((JMenuItem) e.getSource()).getName()) ) {
		ControlInterface.goToMessagegieToNewMessage(this);
		}else if( "Consulter".equals(((JMenuItem) e.getSource()).getName()) ) {
			System.out.println(e.getSource());
			ControlInterface.goToMessagerieToProfil_ConsultOrUpdate(this, MoteurMessagerie.profil, false);
		}else if( "Modifier".equals(((JMenuItem) e.getSource()).getName()) ) {
			ControlInterface.goToMessagerieToProfil_ConsultOrUpdate(this, MoteurMessagerie.profil, true);
		}else if( "Help".equals(((JMenuItem) e.getSource()).getName()) ) {
			//Créer boitde dialogue
		}
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {
		
		if (e.getSource() instanceof JList) {
			
			actionList(e);
//			int index = ((JList)e.getSource()).getSelectedIndex();
//			System.out.println(index);
//			Message msg = modelReception.getElementAt(index);
//			System.out.println(msg.isLu());
//			
//			System.out.println((Message)listeReception.getSelectedValue());
//			msg.setLu(true);
//			modelReception.setElementAt(msg, index);
//			listeReception.setModel(modelReception);
//          listeReception.setCellRenderer(this);
		}	
	}
	
	/**
	 * Permet de récupérer le message sur lequel l'utilisateur a cliqué. Renvoie la JFrame du message et update le msg s'il est en non lu
	 * @param e
	 */
	public  void actionList(MouseEvent e) {
		int index = ((JList)e.getSource()).getSelectedIndex();
		System.out.println(index);
		Message msg = modelReception.getElementAt(index);
		System.out.println(msg);
		ControlInterface.goToMessagerieToMessage(this, msg);
	}
	
	/*----------------------------------------------------------------METHODE OVERRIDEE-------------------------------------------------------------------------------------------------------------------*/
	
	@Override
	public void valueChanged(ListSelectionEvent e) {
		System.out.println(((JList)e.getSource())); 	
	}

	@Override
	public void itemStateChanged(ItemEvent it) {
		System.out.println(it.getSource());
	}

	public static void main(String [] args) {
		new MessagerieUtilisateur();
	}

	@Override
	public Component getListCellRendererComponent(JList list, Object message, int arg2, boolean arg3, boolean arg4) {
		if(arg3) {
			setBackground(list.getSelectionBackground());
			list.setFont(new java.awt.Font(Font.SERIF, Font.ITALIC, 30));
		}
		if(((Message)message).isLu()) {
			list.setFont(new java.awt.Font(Font.SERIF, Font.ITALIC, 30));
			list.setOpaque(false);
			list.setSelectionBackground(Color.BLACK);
		}
		return list;
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}	
}