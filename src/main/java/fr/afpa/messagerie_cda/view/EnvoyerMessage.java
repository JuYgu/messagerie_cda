package fr.afpa.messagerie_cda.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import fr.afpa.messagerie_cda.control.ControlMessage;

public class EnvoyerMessage extends JFrame implements ActionListener, KeyListener, WindowListener {
/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
// composants
	JPanel zoneA;
		JLabel lblA;
		JTextField txA;
	JPanel zoneObjet;
		JLabel lblObjet;
		JTextField txObjet;
	JPanel zoneMessage;
		JTextArea taMessage;
	

JButton btEnvoyer;
JLabel texConf;

//Creation des Pannels
	JPanel p;
	JPanel p1;
	JPanel p2;
	JPanel p3;
	JPanel p4;
	JPanel p5;

//messages
private static String titreAppli="CDA Messagerie";
private static String labelConfirm="Votre compte a été crée avec succès";
private static String lblBtEnvoyer="Envoyer";
	
	public EnvoyerMessage() throws HeadlessException {
	super(titreAppli);
	
	this.texConf=new JLabel(lblBtEnvoyer);
	this.btEnvoyer=new JButton(lblBtEnvoyer);
	
	//instanciation des panels
	this.p=new JPanel();
	this.p1=new JPanel();
	this.p2=new JPanel();
	this.p3=new JPanel();
	this.p4=new JPanel();
	this.p5=new JPanel();
	
	//Decoupade de la frame
	
	
	this.setLayout(new BorderLayout());
	p.setLayout(new BorderLayout());
	
	p2.add(Box.createGlue());
	p3.add(Box.createGlue());
	p4.add(Box.createGlue());
	p5.add(Box.createGlue());
	p.add(p2,BorderLayout.WEST);
	p.add(p3,BorderLayout.EAST);
	p.add(p3,BorderLayout.NORTH);
	p.add(p5,BorderLayout.SOUTH);
	
	TitledBorder envoye = BorderFactory.createTitledBorder("Nouveau Message");
	p.setBorder(envoye);
	p1.setLayout(new GridLayout(10,2));
	
	zoneA = new JPanel(new GridLayout(1,2));
	lblA = new JLabel("A :");
	txA = new JTextField(100);
	zoneA.add(lblA);
	zoneA.add(txA);
	p1.add(zoneA);
	
	zoneObjet = new JPanel(new GridLayout(1,2));
	lblObjet = new JLabel("Objet :");
	txObjet = new JTextField(100);
	zoneObjet.add(lblObjet);
	zoneObjet.add(txObjet);
	p1.add(zoneObjet);
	
	zoneMessage = new JPanel(new GridLayout(1,1));
	taMessage = new JTextArea(10,30);
	zoneMessage.add(taMessage);
	p1.add(zoneMessage);
	
	p1.add(btEnvoyer);
	

	p.add(p1,BorderLayout.CENTER);
	this.add(p,BorderLayout.CENTER);
	
	this.setSize(1000, 750);
	this.setVisible(false);
	setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	// Récupération de la resolution de l'écran
	Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
	this.setLocation((screen.width - this.getSize().width)/2,(screen.height - this.getSize().height)/2);
	setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	
	btEnvoyer.addActionListener(this);
	
}

	@Override
	public void windowActivated(WindowEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowClosed(WindowEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowClosing(WindowEvent arg0) {
		// TODO Auto-generated method stub
		System.exit(0);
	}

	@Override
	public void windowDeactivated(WindowEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowDeiconified(WindowEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowIconified(WindowEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowOpened(WindowEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyPressed(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		if ( ((JButton)arg0.getSource()).getText().equals("Envoyer")){
			//ControlUtilisateur cUser = new ControlUtilisateur();
			System.out.println(txA.getText()) ;
			System.out.println(txObjet.getText()) ;
			System.out.println(taMessage.getText()) ;
			//cUser.checkAuth(loginSaisi, mdpSaisi);
			ControlMessage cuMessage = new ControlMessage();
			boolean retour = cuMessage.controlCreationMail(txObjet.getText(),taMessage.getText(),txA.getText(),MoteurMessagerie.currentUser.getMail());
			if (retour==false) {
				System.out.println("Le message n'a pas été envoyé");
			}
			else
			{
				if(MoteurMessagerie.envoieMessageReussi!=null)
				{
					MoteurMessagerie.envoieMessageReussi.setVisible(true);
				}
				else
				{
					MoteurMessagerie.envoieMessageReussi = new EnvoyerMessageReussi();
					MoteurMessagerie.envoieMessageReussi.setVisible(true);
				}
				this.setVisible(false);
			}
		}
	}
	
	public static void  main(String [] args) {
		EnvoyerMessage EnvoyerMessage = new EnvoyerMessage();	
	}
}