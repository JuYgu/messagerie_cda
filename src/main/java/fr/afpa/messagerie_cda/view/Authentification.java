package fr.afpa.messagerie_cda.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import fr.afpa.messagerie_cda.control.ControlInterface;
import fr.afpa.messagerie_cda.control.ControlUtilisateur;

public class Authentification extends JFrame implements WindowListener, ActionListener {

	// Mise en place des panels :
	private JPanel fenetre;
	
	// nom de la messagerie
	private JPanel nomMessagerie;
	private JLabel cdaMessagerie20156;
	
	// login/mdp incorrect (setVisible(false))
	private JDialog failed;
	private JLabel connexionFailed;
	
	// login & mot de passe, avec zone de texte
	private JPanel logMdp;
	private JLabel log;
	private JLabel mdp;
	private JTextField login;
	private JPasswordField password;
	
	// boutons création de compte + connexion
	private JPanel buttons;
	private JButton checkConnexion, createAccount;
	
	/**
	 * @author Thomas
	 * Première frame visible au lancement de l'application : authentification
	 */
	public Authentification() {
	super("CDA Messagerie");
	//setLayout(new BorderLayout());
	
		// préparation de la fenêtre (en BorderLayout en cas de besoin)
		fenetre = new JPanel(new BorderLayout());
	
		
		// affichage du login/mdp incorrect en cas de mauvaise authentification
		//failed = new JPanel(new FlowLayout(FlowLayout.CENTER, 0, 50));
		//connexionFailed = new JLabel("Login / Mot de passe incorrect !");
		//connexionFailed.setToolTipText("Login / Mot de passe incorrect !");
		//connexionFailed.setFont((new Font(Font.SERIF, Font.BOLD, 30)));
		//connexionFailed.setForeground(new Color(255,0,0));
		//failed.setVisible(false);
		//failed.add(connexionFailed);
		//failed.setBackground(Color.green);
		//this.add(failed,BorderLayout.NORTH);
		
		 failed = new JDialog(this, "Error", true);
		 connexionFailed = new JLabel("Login / Mot de passe incorrect !",JLabel.CENTER);
		 connexionFailed.setFont((new Font(Font.SERIF, Font.BOLD, 30)));
		 connexionFailed.setForeground(new Color(255,0,0));
		 failed.add(connexionFailed);
		 failed.setSize(500,200);
		 Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		 failed.setLocation((screen.width - failed.getSize().width)/2,(screen.height - failed.getSize().height)/2);
		 failed.setVisible(false);
		

				
		// affichage du nom de la messagerie
		nomMessagerie = new JPanel(new FlowLayout(FlowLayout.CENTER, 0, 50));
		cdaMessagerie20156 = new JLabel("CDA 20156 - Messagerie");
		cdaMessagerie20156.setToolTipText("CDA 20156 - Messagerie");
		cdaMessagerie20156.setFont((new Font(Font.SERIF, Font.BOLD, 30)));
		nomMessagerie.add(cdaMessagerie20156);
		//nomMessagerie.setBackground(Color.red);
		this.add(nomMessagerie,BorderLayout.NORTH);
	
		// affichage demande de login mdp + zones de texte en grille 2,2
		logMdp = new JPanel(new GridLayout(2, 2));
		logMdp.setMaximumSize(new Dimension(30, 30));

		//logMdp.setBackground(Color.red);
		log = new JLabel("Login");
		log.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		log.setToolTipText("Login");
		log.setFont((new Font(Font.SERIF, Font.BOLD, 20)));
		
		mdp = new JLabel("Mot de passe");
		mdp.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		mdp.setToolTipText("Mot de passe");
		mdp.setFont((new Font(Font.SERIF, Font.BOLD, 20)));
		login = new JTextField("",10);
		password = new JPasswordField("",10);
		logMdp.add(log);
		logMdp.add(login);
		logMdp.add(mdp);
		logMdp.add(password);
		this.add(logMdp,BorderLayout.CENTER);
	
		// affichage des boutons de création de compte et de connexion
		buttons = new JPanel(new FlowLayout(FlowLayout.CENTER, 0, 50));
		//buttons.setBackground(Color.green);
		createAccount = new JButton("Création de compte");
		checkConnexion = new JButton("Connexion");
		buttons.add(createAccount);
		buttons.add(checkConnexion);
		this.add(buttons,BorderLayout.SOUTH);
	
		checkConnexion.addActionListener(this);
		createAccount.addActionListener(this);
		init();
	}	

	public void init() {
		/*INITATIONATION PARAM FENETRE*/
		this.setSize(1000, 750);
		this.setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// Récupération de la resolution de l'écran
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		this.setLocation((screen.width - this.getSize().width)/2,(screen.height - this.getSize().height)/2);
	}

	@Override
	public void windowActivated(WindowEvent arg0) {
		// TODO Auto-generated method stub
	
	}


	@Override
	public void windowClosed(WindowEvent arg0) {
		// TODO Auto-generated method stub
	
	}


	@Override
	public void windowClosing(WindowEvent arg0) {
		// TODO Auto-generated method stub
		System.exit(-1);
	}


	@Override
	public void windowDeactivated(WindowEvent arg0) {
		// TODO Auto-generated method stub
	
	}


	@Override
	public void windowDeiconified(WindowEvent arg0) {
		// TODO Auto-generated method stub
	
	}


	@Override
	public void windowIconified(WindowEvent arg0) {
		// TODO Auto-generated method stub
	
	}


	@Override
	public void windowOpened(WindowEvent arg0) {
		// TODO Auto-generated method stub
	
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		if ( ((JButton)arg0.getSource()).getText().equals("Connexion")){
			ControlUtilisateur cUser = new ControlUtilisateur();
			String loginSaisi = login.getText() ;
			String mdpSaisi = password.getText();
			if(cUser.checkAuth(loginSaisi, mdpSaisi)) {
				ControlInterface.goToAuthToMessagerie(this, MoteurMessagerie.messagerieUser);
			} else {
				//nomMessagerie.setVisible(false);
				 failed.setVisible(true);
			}
		} else if( ((JButton)arg0.getSource()).getText().equals("Création de compte")){
			// Mettre la méthode de création de compte
			MoteurMessagerie.creaCompte = new CreationDeCompte();
			MoteurMessagerie.creaCompte.setVisible(true);
			this.setVisible(false);
		}
	}
}