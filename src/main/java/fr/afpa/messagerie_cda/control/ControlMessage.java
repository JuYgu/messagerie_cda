package fr.afpa.messagerie_cda.control;

import fr.afpa.messagerie_cda.beans.Utilisateur;
import fr.afpa.messagerie_cda.model.GestionMessage;
import fr.afpa.messagerie_cda.model.GestionUtilisateur;

import java.util.List;

import fr.afpa.messagerie_cda.Main;
import fr.afpa.messagerie_cda.beans.Message;
import fr.afpa.messagerie_cda.model.GestionMessage;

public class ControlMessage {
	
	/**
	 * Methode de control envoie message
	 * @param objet
	 * @param corps
	 * @param destinataire
	 * @param emetteur
	 * @return
	 */
	public boolean controlCreationMail(String objet,String corps,String destinataire,String emetteur) {
		if (destinataire.equals(null)) {
			return false;
		}else if (objet.equals(null)) {
			return false;
		}else if (corps.equals(null)) {
			return false;
		}
		
		GestionUtilisateur gu = new GestionUtilisateur();
		
		Utilisateur user = new Utilisateur();
		user.setMail(destinataire);
		
		Utilisateur utilisateur = gu.rechercherUtilisateur(user);
		
		
		if (utilisateur == null) {
			return false;
		}else {
		GestionMessage geEnvoieMessage = new GestionMessage();
		//geEnvoieMessage.metierEnvoieMail(objet,corps,destinataire,emetteur);
		if (geEnvoieMessage.metierEnvoieMail(objet, corps, destinataire, emetteur)==false) {
		}
		return true;
		}
	}
	
	/*-----------------------------------------------------------------------------CONCEPTION MESSAGERIE-------------------------------------------------------------------------------*/
	
	
	/***
	 * Retourne la liste des messages obtenu avec l'objet de recherche "objet" mis en parametre
	 * @param objet
	 * @return
	 */
 	public List<Message> CgetSearchingMsg(String objet){
 		GestionMessage gMsg = new GestionMessage();
 		return gMsg.getSearchingMsg(objet);
 	}
 	
 	
 	/**
 	 * Retourne tous les messages reçus du current user
 	 * @return
 	 */
 	public List<Message> getAllReceptMsg(){
 		GestionMessage gMsg = new GestionMessage();
 		return gMsg.getAllReceptMsg();
 	}
 	
 	/**
 	 * Retourne tous les messages reçus du current user
 	 * @return
 	 */
 	public List<Message> getAllSendMsg(){
 		GestionMessage gMsg = new GestionMessage();
 		return gMsg.getAllSendtMsg();
 	}
 	
 	/**
 	 * Calcul et retourne le nombre de message non lus
 	 * @return
 	 */
	public int getCounterUnreadMsg() {
		List<Message> list =  Main.userBase.getMsgRecus();
		int counter = 0;
		for ( Message msg : list) {
			if( !msg.isLu()) {
				counter++;
			}
		}
		return counter;
	}

	public void updateMsg(Message msg) {
		msg.setLu(true);
		GestionMessage gMsg = new GestionMessage();
		gMsg.updateMsg(msg);
	}
}