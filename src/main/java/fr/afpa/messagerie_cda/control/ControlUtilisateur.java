package fr.afpa.messagerie_cda.control;

import java.util.List;
import java.util.Scanner;

import fr.afpa.messagerie_cda.Main;
import fr.afpa.messagerie_cda.beans.Message;
import fr.afpa.messagerie_cda.beans.Utilisateur;
import fr.afpa.messagerie_cda.model.GestionUtilisateur;
import fr.afpa.messagerie_cda.view.MoteurMessagerie;

public class ControlUtilisateur {
	
	/**
	 * control de la syntaxe des champs pour la creation de compte
	 * @param nom
	 * @param prenom
	 * @param mail
	 * @param tel
	 * @param login
	 * @param mdp
	 * @return
	 */
	public boolean controlVerifSyntax(String nom, String prenom,String mail, String tel,String login,String mdp) {
		if (nom.equals(null)||nom.length()<2||nom.length()>30) {
			return false;
		} else if(prenom.equals(null)||prenom.length()<2||prenom.length()>30) {
			return false;
		
		}else if(mail.equals(null)) {
			return false;
		}else if(tel.equals(null)) {
			return false;
		}else if(login.equals(null)) {
			return false;
		}else if(mdp.equals(null)) {
			return false;
		}
		GestionUtilisateur gu = new GestionUtilisateur();
		
		if (gu.creationCompte(nom, prenom, mail, tel, login, mdp)==false) {
			return false;
		}
		
		return true;
		}
	
	/*-----------------------------------------------------------------------------CONCEPTION AUTH      -------------------------------------------------------------------------------*/
	// check auth (qui renvoie bool) qui appelle checkuserisexist
			// si true : appelle de la méthode checkMdp
			// si checkMdp == true alors return true;
			// return false
		
	/**
	* @author Thomas
	* Vérification de l'authentification de l'utilisateur sur l'application :
	* appelle la méthode checkUserIsExists() : si true, appelle de la méthode checkMdp()
	* 
	* @param
	* @return true si checkMdp return true, sinon retourne false
	*/
	public boolean checkAuth(String login, String password) {
		GestionUtilisateur gUser = new GestionUtilisateur();
		Scanner in = new Scanner (System.in);
		List<Utilisateur> ListUserByLogin = gUser.GestiongetUserByLogin(login);
		if (checkUserIsExist(login)==false) {
			System.out.println("login / mot de passe incorrect !");
		} else {		
			
				if (MoteurMessagerie.currentUser.getPassword().equals(password)) { 
					return true;
				} else {
					System.out.println("login / mot de passe incorrect !");
				}
			}
		return false;
	}
			
	
	/*-----------------------------------------------------------------------------CONCEPTION MESSAGERIE-------------------------------------------------------------------------------*/
	
	
	//-------------------> A FAIRE : CHANGER le MAIN.userBase par MOTEURMESSAGERIE.currentUser
	/**
	 * Verifie si un utilisateur est bien trouvé grace au login donné en parametre
	 * Si c'est le cas, elle instancie l'utilisateur static dans le MOTEURMESSAGERIE et retourne True, sinon retourne false
	 * @param login
	 * @return
	 */
	public boolean checkUserIsExist(String login) {
		GestionUtilisateur gUser = new GestionUtilisateur();
		List<Utilisateur> ListUserByLogin = gUser.GestiongetUserByLogin(login);
		if (ListUserByLogin.size() == 0) {
	 		return false;
	 	}else {
	 		Main.userBase = ListUserByLogin.get(0);
	 		MoteurMessagerie.currentUser = ListUserByLogin.get(0);	 	
	 		return true;
	 	}
	}
	
	
	/*-----------------------------------------------------------------------------CONCEPTION PROFIL-------------------------------------------------------------------------------*/

	
	public boolean updateUser(String password, String nom, String prenom, String noPhone) {
		GestionUtilisateur gUser = new GestionUtilisateur();
		
		return gUser.updateUser( password,  nom,  prenom,  noPhone);
	}
	
	public boolean checkSyntax(String nom, String prenom,String tel,String mdp) {
		System.out.println(nom);
		if (nom.equals(null)||nom.length()<2||nom.length()>30) {
			return false;
		} 
		if(prenom.equals(null)||prenom.length()<2||prenom.length()>30) {
			return false;
		}
		if(tel.equals(null)) {
			return false;
		}
		if(mdp.equals(null)) {
			return false;
		}
		GestionUtilisateur gu = new GestionUtilisateur();
		
		if (gu.updateUser(nom, prenom, tel,  mdp)==false) {
			return false;
		}
		return true;
		}
}