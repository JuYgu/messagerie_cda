package fr.afpa.messagerie_cda.control;

import javax.swing.JFrame;

import fr.afpa.messagerie_cda.beans.Message;
import fr.afpa.messagerie_cda.view.Authentification;
import fr.afpa.messagerie_cda.view.EnvoyerMessage;
import fr.afpa.messagerie_cda.view.EnvoyerMessageReussi;
import fr.afpa.messagerie_cda.view.MessagerieUtilisateur;
import fr.afpa.messagerie_cda.view.MoteurMessagerie;
import fr.afpa.messagerie_cda.view.Profil;
import fr.afpa.messagerie_cda.view.ReadingMessage;

public interface ControlInterface {

	/**
	 * Methode permettant d'aller du profil a la messagerie
	 * @param profil
	 * @param messagerieUser
	 */
	public static void goToProfilToMessagerie(Profil profil, MessagerieUtilisateur messagerieUser) {
		MoteurMessagerie.messagerieUser.refresh();
		messagerieUser.toFront();
		messagerieUser.setVisible(true);
		profil.setVisible(false);	
	}
	
	/**
	 * Permet d'aller de la messagerie a un nouveau message
	 */
	public static void goToMessagegieToNewMessage( MessagerieUtilisateur messagerieUser) {
		EnvoyerMessage newMsg = new EnvoyerMessage();
		newMsg.setVisible(true);
		messagerieUser.setVisible(false);	
	}
	
	/**
	 * Permet d'aller consulter son profil ou entamer sa modification si le boolean est a true ou false (true : chemin modifier, false chemin consulter)
	 * @param messagerieUser
	 * @param profil
	 * @param update
	 */
	public static void goToMessagerieToProfil_ConsultOrUpdate(MessagerieUtilisateur messagerieUser, Profil profil, boolean update) {
		profil.refresh(update);
		profil.toFront();
		profil.setVisible(true);
		messagerieUser.setVisible(false);	
	}
	
	/**
	 * Permer d'aller de la messagerie à l'authentification : déconnexion
	 * @param messagerieUser
	 * @param auth
	 */
	public static void goToMessagegieToAuth(MessagerieUtilisateur messagerieUser) {
		MoteurMessagerie.auth = new Authentification();
		messagerieUser.setVisible(false);	
	}
	
	/**
	 * Permet d'aller consulter un message. Si celui ci n'est pas encore lu, change sa valeur dans la base de donnée
	 * @param messagerieUser
	 * @param readingMsg
	 * @param msg
	 */
	public static void goToMessagerieToMessage(MessagerieUtilisateur messagerieUser,Message msg) {
		MoteurMessagerie.readingMsg = new ReadingMessage();
		MoteurMessagerie.readingMsg.setCorpsText(msg.getCorps());
		MoteurMessagerie.readingMsg.setEmetteur(msg.getEmetteur().getLogin());
		MoteurMessagerie.readingMsg.setObjectText(msg.getObjet());
		MoteurMessagerie.readingMsg.initMsg();
		
		MoteurMessagerie.readingMsg.setVisible(true);
		messagerieUser.setVisible(false);
		if(! msg.isLu()) {
			ControlMessage cMsg =  new ControlMessage();
			cMsg.updateMsg(msg);
		}
	}
	
	/**
	 * Permet d'aller du message lu a la boite de messagerie
	 * @param readingMsg
	 * @param msg
	 * @param messagerieUser
	 */
	public static void goToReadMessToMessagerie(ReadingMessage readingMsg, MessagerieUtilisateur messagerieUser) {
		MoteurMessagerie.messagerieUser.refresh();
		messagerieUser.setVisible(true);
		readingMsg.dispose();
	}
	
	/**
	 * Méthode qui permet d'aller de la lecture d'un message à l'authentification
	 * @param readingMsg
	 * @param auth
	 */
	
	public static void goToMessageToAuth(ReadingMessage readingMsg, Authentification auth) {
		//MoteurMessagerie.auth.dispose();
		MoteurMessagerie.auth = new Authentification();
		
        auth.setVisible(true);
        auth.dispose();
        readingMsg.dispose();
    }
		
	/**
	 * Permet d'aller de l'auth  a la boite de messagerie
	 * @param readingMsg
	 * @param msg
	 * @param messagerieUser
	 */
	public static void goToAuthToMessagerie(Authentification auth , MessagerieUtilisateur messagerieUser) {
		messagerieUser = new MessagerieUtilisateur();
		MoteurMessagerie.messagerieUser = messagerieUser;
		MoteurMessagerie.profil = new Profil();
		messagerieUser.setVisible(true);
		//MoteurMessagerie.auth.dispose();
		auth.dispose();
	}
		
	public static void goToMessageSuccessToMessagerie(EnvoyerMessageReussi successMsg){
		MoteurMessagerie.messagerieUser.refresh();
	}
}