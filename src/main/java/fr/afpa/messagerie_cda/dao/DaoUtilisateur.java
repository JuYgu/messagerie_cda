package fr.afpa.messagerie_cda.dao;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.Transaction;

import fr.afpa.messagerie_cda.Main;
import fr.afpa.messagerie_cda.beans.Message;
import fr.afpa.messagerie_cda.beans.Utilisateur;
import fr.afpa.messagerie_cda.session.HibernateUtils;

public class DaoUtilisateur {
/**
 * permer de creer ou de rechercher un utilisateur existant
 * @param utilisateur
 * @return
 */
	public boolean enregistrerPersonne(Utilisateur utilisateur) {
		// Creation d une session
		Session s ;
		//Creation d une session hibernate
		s= HibernateUtils.getSession();
		//Debut de la transaction
		Transaction tx=s.beginTransaction();
		Query q = s.getNamedQuery("Verif_utilisateur");
		q.setParameter("login", utilisateur.getLogin());
		
		//Enregistrement de l objet Utilisateur
		if (q.getResultList().size()!=0) {
			
			return false;
		}

		s.save(utilisateur);
		tx.commit();

		return true;
	}
	
	/**
	 * permet de creer ou de rechercher un utilisateur existant
	 * @param utilisateur
	 * @return
	 */
		public Utilisateur recupPersonne(Utilisateur utilisateur) {
			// Creation d une session
			Session s ;
			//Creation d une session hibernate
			s= HibernateUtils.getSession();
			//Debut de la transaction
			Transaction tx=s.beginTransaction();
			Query q = s.getNamedQuery("findUserByLoginOrMail");
			q.setParameter("login", utilisateur.getLogin());
			q.setParameter("mail", utilisateur.getMail());
			
			ArrayList<Utilisateur> listeUtilisateurs = (ArrayList<Utilisateur>)q.getResultList();
			
			tx.commit();
			s.close();
			
			//Enregistrement de l objet Utilisateur
			if (listeUtilisateurs!=null && listeUtilisateurs.size()>0 && listeUtilisateurs.get(0) != null) {
				return listeUtilisateurs.get(0);
			}
			return null;
		}
	
	/*-----------------------------------------------------------------------------CONCEPTION MESSAGERIE-------------------------------------------------------------------------------*/
	
	/**
	 * Permet de chercher l'utilisateur grace a son login mit en parametre
	 * renvoie l'utilisateur trouvé
	 * @param login
	 * @return
	 */
	public static List<Utilisateur> getUserByLogin(String login) {
		//Se connecter a la session
		Session s = HibernateUtils.getSession();
		Transaction tx = s.beginTransaction();
		Query q  = s.getNamedQuery("findUserByLogin");
		q.setParameter("login", login);
		
		ArrayList<Utilisateur> user = null;
		System.out.println();
		user = (ArrayList<Utilisateur>) q.getResultList();
		tx.commit();
		return user;
	}
	
	
	/*-----------------------------------------------------------------------------CONCEPTION PROFIL-------------------------------------------------------------------------------*/

	
	public boolean updateUser(Utilisateur user) {

		Session s = HibernateUtils.getSession();
		Transaction tx = s.beginTransaction();
		s.merge(user);
		tx.commit();
		s.close();
				
		return true;
	}
}