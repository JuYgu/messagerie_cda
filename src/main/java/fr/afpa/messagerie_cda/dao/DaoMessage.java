package fr.afpa.messagerie_cda.dao;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.Transaction;

import fr.afpa.messagerie_cda.beans.Message;
import fr.afpa.messagerie_cda.beans.Utilisateur;
import fr.afpa.messagerie_cda.model.GestionMessage;
import fr.afpa.messagerie_cda.session.HibernateUtils;

public class DaoMessage {
	/**
	 * Methode pour envoyer un message
	 * @param envoieMessage
	 * @return
	 */
	public boolean envoieMessage(Message envoieMessage) {
		
		// Creation d une session
		Session s ;
		//Creation d une session hibernate
		s= HibernateUtils.getSession();
		//Debut de la transaction
		Transaction tx=s.beginTransaction();
		//Enregistrement de l objet Utilisateur
		s.save(envoieMessage);
		tx.commit();
		s.close();
		return true;
	}
	
	public void updateMsg(Message msg) {
		Session s = HibernateUtils.getSession();
		Transaction tx = s.beginTransaction();
		s.merge(msg);
		tx.commit();
		s.close();
	}
}