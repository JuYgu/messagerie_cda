package fr.afpa.messagerie_cda.model;

import fr.afpa.messagerie_cda.Main;
import fr.afpa.messagerie_cda.beans.Message;
import java.util.List;

import fr.afpa.messagerie_cda.beans.Utilisateur;
import fr.afpa.messagerie_cda.dao.DaoMessage;
import fr.afpa.messagerie_cda.dao.DaoUtilisateur;

public class GestionUtilisateur {
	/**
	 * Creation methode pour la creation de compte
	 * @param nom
	 * @param prenom
	 * @param mail
	 * @param tel
	 * @param login
	 * @param mdp
	 * @return
	 */
	public boolean creationCompte(String nom, String prenom,String mail, String tel,String login,String mdp) {
		DaoUtilisateur nouveauCompte = new DaoUtilisateur();
		Utilisateur nouvelUtilisateur = new Utilisateur(nom, prenom, mail, tel, login, mdp);
		
		if (nouveauCompte.enregistrerPersonne(nouvelUtilisateur)==false) {
			return false;
		}
		
		return true;
	
	}
	
	
	
	/**
	 * Permet de recuperer un utilisateur ou un null pour la couche sup control 
	 * @param user
	 * @return
	 */
	public Utilisateur rechercherUtilisateur(Utilisateur user) {
		DaoUtilisateur daoUser = new DaoUtilisateur();
		
		Utilisateur userBdd = daoUser.recupPersonne(user);
		return userBdd;
		
	}
	

	
	
	/*-----------------------------------------------------------------------------CONCEPTION MESSAGERIE-------------------------------------------------------------------------------*/
	
	/**
	 * Appelle la méthode getUserByLogin en couche DAO et renvoie la liste retournée par cette méthode
	 * @param login
	 * @return
	 */
	public List<Utilisateur> GestiongetUserByLogin(String login){
		DaoUtilisateur DaoUser = new DaoUtilisateur();
  	 	return  DaoUser.getUserByLogin(login);
	}
		
	/*-----------------------------------------------------------------------------CONCEPTION PROFIL-------------------------------------------------------------------------------*/

	public boolean updateUser(String password, String nom, String prenom, String noPhone) {
		DaoUtilisateur uDao = new DaoUtilisateur();
		
		Utilisateur user = Main.userBase;
		user.setPassword(password);
		user.setNom(nom);
		user.setPrenom(prenom);
		user.setNoPhone(noPhone);
		System.out.println(user.getId_utilisateur());
		
		return uDao.updateUser(user );	
	}
}