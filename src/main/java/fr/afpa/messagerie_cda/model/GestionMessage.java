package fr.afpa.messagerie_cda.model;

import fr.afpa.messagerie_cda.beans.Message;
import fr.afpa.messagerie_cda.beans.Utilisateur;
import fr.afpa.messagerie_cda.dao.DaoMessage;
import fr.afpa.messagerie_cda.dao.DaoUtilisateur;

import java.util.ArrayList;
import java.util.List;

import fr.afpa.messagerie_cda.Main;
import fr.afpa.messagerie_cda.beans.Message;

public class GestionMessage {
	/**
	 * creation methode pour envoyer mail 
	 * 
	 * @param objet
	 * @param corps
	 * @param destinataire
	 * @param emetteur
	 * @return
	 */
	
	public boolean metierEnvoieMail(String objet,String corps,String destinataire,String emetteur) {
		DaoMessage daoEnvoieMail = new DaoMessage();
		
		Utilisateur utDestinataire = new Utilisateur();
		//Action de setter tous les attributs de l utilisateur pour instanciatier l utilisateur
		//Permet de recuperer le mail mis dans les parametres et eviter le non null exception
		GestionUtilisateur gu = new GestionUtilisateur();
		utDestinataire.setMail(destinataire);
		utDestinataire.setLogin("");
		//		Permet de recuperer toutes les données du destinataire de la base de donnée et l affecter a la personne
		utDestinataire = gu.rechercherUtilisateur(utDestinataire);
				
		//instanciation de l'utilisateur et remplissage des champs
		Utilisateur utEmetteur = new Utilisateur();
		//valeur qui nous interresse
		//Permet de recuperer toutes les données de l emetteur de la base de donnée et l affecter a la personne
		utEmetteur.setMail(emetteur);
		utEmetteur.setLogin("");
				
		utEmetteur = gu.rechercherUtilisateur(utEmetteur);
				
		Message envoieMail = new Message(objet,corps,utDestinataire,utEmetteur);
		//Permet d envoyer un mail
		daoEnvoieMail.envoieMessage(envoieMail);
		return true;
			
	}
	
	/*-----------------------------------------------------------------------------CONCEPTION MESSAGERIE-------------------------------------------------------------------------------*/
	
	/**
	 * Renvoie tous les message reçus du currentUser
	 * @return
	 */
	public List<Message> getAllReceptMsg(){
		return Main.userBase.getMsgRecus();
	}

	/**
	 * Renvoie tous les messages envoyés du currentUser
	 * @return
	 */
	public List<Message> getAllSendtMsg(){
		return Main.userBase.getMsgEnvoyes();
	}
	
	
	/**
	 * Récupère la liste des messages reçus et envoyés, appelle la méthode de recherche et renvoie la liste de message trouvés en fonction de la recherche
	 * @param objet
	 * @return
	 */
 	public List<Message> getSearchingMsg(String objet){
 		List<Message> lMsgRecus = Main.userBase.getMsgRecus();
 		List<Message> lMsgSend = Main.userBase.getMsgEnvoyes();
 		return listMessageByObject(objet, lMsgRecus, lMsgSend);
 	}
	
	/**
	 * Permet de selectionner et d'implémenter une liste contenant les messages dont l'objet contient la recherche mise en parametre. 
	 * prend en parametre la liste de msg reçu et la liste de message envoyé
	 * retourne la liste contenant les Objets message trouvé.
	 * 
	 * @param objet
	 * @param msgRecu
	 * @param msgSend
	 * @return
	 */
	public static List<Message> listMessageByObject(String objet, List<Message> msgRecu, List<Message> msgSend){
		List<Message> listeRecherchee = new ArrayList<Message>();
		
		objet = objet.toLowerCase();
		
		for (Message msg : msgRecu) {
			System.out.println(msg.getObjet());

			if(msg.getObjet().toLowerCase().contains(objet)) {
				listeRecherchee.add(msg);
			}
		}
		
		for (Message msg : msgSend) {
			System.out.println(msg.getObjet());
			if(msg.getObjet().toLowerCase().contains(objet)) {
				listeRecherchee.add(msg);
			}
		}
		System.out.println(listeRecherchee.size());
		return listeRecherchee;			
	}
	
	public void updateMsg(Message msg) {
		DaoMessage daoMsg = new DaoMessage();
		daoMsg.updateMsg(msg);
	}
}