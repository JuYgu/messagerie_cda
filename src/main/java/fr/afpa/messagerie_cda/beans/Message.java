package fr.afpa.messagerie_cda.beans;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * Configuration hibernate pour communiquer avec la base de données / Table Message
 * Requêtes : findUserByLogin + findUserByLoginOrMail + Verif_utilisateur
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
//@ToString(of = {"idMessage", "objet", "corps"})

@Entity
public class Message {
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "message_id_generator")
	@SequenceGenerator(name = "message_id_generator", sequenceName = "message_seq", allocationSize = 1, initialValue = 1)
	@Column(name = "id_message")
	private int idMessage;
	
	@Column(nullable = false, length= 30)
	private String objet;
	
	@Column(nullable = false, columnDefinition ="TEXT")
	private String corps;
	
	@Column( nullable = false)
	private boolean lu = false;
	
	@ManyToOne (fetch = (FetchType.LAZY), cascade = CascadeType.ALL)
	@JoinColumn (name="fk_id_destinataire", referencedColumnName = "id_utilisateur" )
	Utilisateur destinataire;
	
	@ManyToOne (fetch = (FetchType.LAZY), cascade =  CascadeType.ALL)
	@JoinColumn (name="fk_id_emetteur", referencedColumnName = "id_utilisateur")
	Utilisateur emetteur;

	@Override
	public String toString() {
		return emetteur.getLogin() +"	-----------	 	" + objet.toLowerCase() + "		----------------" + corps.substring(0, 10)+"..." +"-------------- Destinataire :" + destinataire.getLogin();
	}

	public Message(String objet, String corps, Utilisateur destinataire, Utilisateur emetteur) {
		
		this.objet = objet;
		this.corps = corps;
		this.destinataire = destinataire;
		this.emetteur = emetteur;
	}
}