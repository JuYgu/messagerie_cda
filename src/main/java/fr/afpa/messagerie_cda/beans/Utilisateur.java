package fr.afpa.messagerie_cda.beans;

import java.util.List;


import javax.persistence.Column;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * Configuration hibernate pour communiquer avec la base de données / Table Utilisateur
 * Requêtes : findUserByLogin + findUserByLoginOrMail + Verif_utilisateur
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString(of = {"id_utilisateur", "password", "nom", "prenom", "mail", "noPhone"})
@NamedQuery(name="Verif_utilisateur",query="select login from Utilisateur u where u.login=:login")

@Entity
@NamedQuery(name = "findUserByLogin", query =  "Select util FROM Utilisateur util where util.login = :login")
@NamedQuery(name = "findUserByLoginOrMail", query =  "Select util FROM Utilisateur util where util.login = :login or util.mail= :mail")
public class Utilisateur{

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "utilisateur_id_generator")
	@SequenceGenerator(name = "utilisateur_id_generator", sequenceName = "utilisateur_seq", allocationSize = 1, initialValue = 1)
	@Column(name="id_utilisateur")
	private int id_utilisateur;
	
	@Column(nullable = false, length= 10, unique= true)
	private String login;
	
	@Column(nullable = false, length= 20)
	private String password;
	
	@Column(nullable = false, length= 30)
	private String nom;
	
	@Column(nullable = false, length= 30)
	private String prenom;
	
	@Column(nullable = false)
	private String mail;
	
	@Column(nullable = false, length= 10)
	private String noPhone;
	
	@OneToMany (mappedBy = "emetteur")
	private List<Message> msgEnvoyes;
	
	@OneToMany (mappedBy = "destinataire")
	private List<Message> msgRecus;

	public Utilisateur(String nom, String prenom,String mail, String noPhone,String login,String password) {
		
		this.login = login;
		this.password = password;
		this.nom = nom;
		this.prenom = prenom;
		this.mail = mail;
		this.noPhone = noPhone;
	}	
}